# Capturas de Pantalla

## Descripción

Esta carpeta contiene capturas de pantalla de las implementaciones, dashboards y resultados de pruebas. Las capturas están organizadas en subcarpetas según el componente o herramienta correspondiente.

## Estructura

### 1_k8s

Capturas de pantalla relacionadas con el despliegue y la gestión de Kubernetes:

- **0_k8s_diagram.png**: Muestra el diagrama de despliegue de los componentes en Kubernetes.
- **1_pods.png**: Muestra los pods desplegados.
- **2_deployments.png**: Muestra los despliegues en Kubernetes.
- **3_stateful_sets.png**: Muestra los Stateful Sets.
- **4_daemon_sets.png**: Muestra los Daemon Sets.
- **5_hpa.png**: Muestra la configuración de Autoescalado Horizontal (HPA).
- **6_services.png**: Muestra los servicios desplegados.

### 2_grafana

Capturas de pantalla de los dashboards de Grafana utilizados para monitoreo:

- **grafana_redis_app_100_parallel_requests.png**: Dashboard de Grafana mostrando métricas de Redis y la aplicación bajo 100 solicitudes paralelas.
- **grafana_redis_app_100_sequential_requests.png**: Dashboard de Grafana mostrando métricas de Redis y la aplicación bajo 100 solicitudes secuenciales.

### 3_kibana

Capturas de pantalla de los dashboards de Kibana utilizados para el análisis de logs:

- **1_logs_kubernetes.png**: Dashboard de Kibana mostrando los logs de Kubernetes.
- **2_logs_filter_spring-boot-app.png**: Dashboard de Kibana mostrando los logs filtrados de la aplicación Spring Boot.

### 4_jmeter

Capturas de pantalla de los resultados de las pruebas de carga realizadas con JMeter:

- **100_parallel_requests_result_table.png**: Tabla de resultados para 100 solicitudes paralelas.
- **100_parallel_requests_summary_report.png**: Informe resumen para 100 solicitudes paralelas.
- **1000_sequential_requests_result_table.png**: Tabla de resultados para 1000 solicitudes secuenciales.
- **1000_sequential_requests_summary_report.png**: Informe resumen para 1000 solicitudes secuenciales.

## Uso de las Capturas de Pantalla

Estas capturas de pantalla son útiles para:

- Verificar y documentar el estado y la configuración del despliegue en Kubernetes.
- Monitorear y analizar el rendimiento de la aplicación y de Redis usando Grafana.
- Analizar y gestionar los logs de la aplicación y de Kubernetes usando Kibana.
- Evaluar el rendimiento de la aplicación bajo diferentes condiciones de carga usando JMeter.

