# Desafío Cloud

## Descripción

Este repositorio contiene el código fuente, archivos de configuración y documentación para el desafío cloud solicitado.

## Estructura del Repositorio

- **app/**: Contiene el código fuente del servicio web.
- **k8s/**: Contiene los archivos de configuración de Kubernetes.
- **screens/**: Contiene capturas de pantalla de las implementaciones, dashboards y resultados de pruebas.
- **test/**: Contiene el archivo de proyecto para de las pruebas.
- **docs/**: Contiene el documento de diseño y la presentación.

## Contenidos

### app/

Esta carpeta contiene el código fuente del servicio web en Java y los archivos de recursos.

### k8s/

Esta carpeta contiene los archivos de configuración de Kubernetes necesarios para desplegar la arquitectura solicitada.

### screens/

Esta carpeta contiene las capturas de pantalla de:
- Dashboards de Grafana
- Logs de Kibana
- Despliegue realizado en Kubernetes
- Resultados de pruebas de JMeter

### test/

Esta carpeta contiene el archivo de configuración de las pruebas de carga con JMeter.

### docs/

Esta carpeta contiene:
- Documento de diseño y decisiones técnicas tomadas (PDF)
- Diapositivas de la presentación (PDF)

## Información de Contacto

- **Nombre**: Edwar A. Gaspar Sánchez
- **Email**: egaspar@edwx.pro
- **LinkedIn**: [www.linkedin.com/in/edwx](https://www.linkedin.com/in/edwx/)