package pro.edwx.api.cloud_challenge.infrastructure.client;

import java.util.Map;

public interface HttpClientService {
    <T> T doGet(String endpoint, Map<String, String> queryParams, Class<T> responseType);
}
