package pro.edwx.api.cloud_challenge.infrastructure.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PokemonApiResponse {

    private Integer id;
    private String name;
    private Sprites sprites;
    private List<Type> types;

    @Getter
    @Setter
    public static class Sprites {

        private String front_default;
    }

    @Getter
    @Setter
    public static class Type {

        private SlotType type;

        @Getter
        @Setter
        public static class SlotType {
            private String name;
        }
    }
}
