package pro.edwx.api.cloud_challenge.application.controller;

import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pro.edwx.api.cloud_challenge.application.dto.PokemonResponse;
import pro.edwx.api.cloud_challenge.application.service.PokemonService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pokemon")
public class PokemonController {

    private final PokemonService pokemonService;

    @GetMapping("/{id}")
    public PokemonResponse getPokemon(@PathVariable @Min(1) Integer id) {
        PokemonResponse response = pokemonService.getPokemon(id);
        return response;
    }
}
