package pro.edwx.api.cloud_challenge.application.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import pro.edwx.api.cloud_challenge.application.dto.PokemonResponse;
import pro.edwx.api.cloud_challenge.application.exception.PokemonNotFoundException;
import pro.edwx.api.cloud_challenge.application.mapper.PokemonMapper;
import pro.edwx.api.cloud_challenge.application.service.PokemonService;
import pro.edwx.api.cloud_challenge.domain.model.Pokemon;
import pro.edwx.api.cloud_challenge.domain.repository.PokemonRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PokemonServiceImpl implements PokemonService {

    private final PokemonRepository pokemonRepository;
    private final PokemonMapper pokemonMapper;

    public PokemonResponse getPokemon(Integer id) {
        return pokemonRepository.getPokemon(id)
            .map(pokemonMapper::toResponse)
            .orElseThrow(PokemonNotFoundException::new);
    }
}
