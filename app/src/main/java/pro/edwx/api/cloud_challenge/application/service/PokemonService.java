package pro.edwx.api.cloud_challenge.application.service;

import pro.edwx.api.cloud_challenge.application.dto.PokemonResponse;

public interface PokemonService {

    PokemonResponse getPokemon(Integer id);
}
