package pro.edwx.api.cloud_challenge.util;

import lombok.Getter;

@Getter
public enum ErrorCatalog {

    INVALID_PARAMETER("ERR_CODE_001", "Parámetro inválido."),
    POKEMON_NOT_FOUND("ERR_CODE_002", "El pokemon no fue encontrado."),
    POKEMON_API_ERROR("ERR_CODE_003", "Error al consultar la API de Pokemon."),
    GENERIC_ERROR("ERR_CODE_004", "Ocurrion un error inesperado.");

    private final String code;
    private final String message;

    ErrorCatalog(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
