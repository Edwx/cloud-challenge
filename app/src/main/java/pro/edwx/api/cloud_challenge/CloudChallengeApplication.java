package pro.edwx.api.cloud_challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudChallengeApplication.class, args);
	}

}
