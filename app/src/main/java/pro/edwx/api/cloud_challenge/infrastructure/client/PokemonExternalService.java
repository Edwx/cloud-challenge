package pro.edwx.api.cloud_challenge.infrastructure.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import org.springframework.web.util.UriComponentsBuilder;
import pro.edwx.api.cloud_challenge.application.exception.ExternalServiceException;

import java.util.Map;

@Component
public class PokemonExternalService implements HttpClientService {

    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${pokemon.base-url}")
    private String baseUrl;

    @Override
    public <T> T doGet(
        String endpoint,
        Map<String, String> queryParams,
        Class<T> responseType
    ) {
        String finalUrl = buildFinalUrl(endpoint, queryParams);

        HttpEntity<Object> httpEntity = new HttpEntity<>(getHeaders());

        ResponseEntity<T> response;
        try {
            response = restTemplate
                .exchange(finalUrl, HttpMethod.GET, httpEntity, responseType);
        } catch (Exception e) {
            throw new ExternalServiceException("Error al obtener el recurso ( {} : {} )", e);
        }

        if (response.getStatusCode().is5xxServerError()) {
            String message = String.format(
                "Error al obtener el recurso ( {} : {} ), código de respuesta: {}",
                HttpMethod.GET, endpoint, response.getStatusCode()
            );
            throw new ExternalServiceException(message, null);
        }

        return response.getBody();
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        return headers;
    }

    private String buildFinalUrl(String endpoint, Map<String, String> queryParams) {
        String url = baseUrl.concat("/").concat(endpoint);

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url);

        if (queryParams != null) {
            queryParams.forEach(uriBuilder::queryParam);
        }

        return uriBuilder.toUriString();
    }
}
