package pro.edwx.api.cloud_challenge.domain.repository;

import java.util.Optional;

import pro.edwx.api.cloud_challenge.domain.model.Pokemon;

public interface PokemonRepository {

    Optional<Pokemon> getPokemon(Integer id);
}
