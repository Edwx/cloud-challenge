package pro.edwx.api.cloud_challenge.application.controller;

import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import pro.edwx.api.cloud_challenge.application.exception.PokemonNotFoundException;
import pro.edwx.api.cloud_challenge.application.exception.ExternalServiceException;
import pro.edwx.api.cloud_challenge.domain.model.ErrorResponse;
import pro.edwx.api.cloud_challenge.util.ErrorCatalog;

import java.time.LocalDateTime;
import java.util.Collections;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public ErrorResponse handleIllegalArgumentException(
        IllegalArgumentException ex
    ) {
        return ErrorResponse.builder()
            .code(ErrorCatalog.INVALID_PARAMETER.getCode())
            .status(HttpStatus.BAD_REQUEST)
            .message(ErrorCatalog.INVALID_PARAMETER.getMessage())
            .detailMessages(Collections.singletonList(ex.getMessage()))
            .timestamp(LocalDateTime.now())
            .build();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(PokemonNotFoundException.class)
    public ErrorResponse handlePokemonNotFoundException(
        PokemonNotFoundException ex
    ) {
        return ErrorResponse.builder()
            .code(ErrorCatalog.POKEMON_NOT_FOUND.getCode())
            .status(HttpStatus.NOT_FOUND)
            .message(ErrorCatalog.POKEMON_NOT_FOUND.getMessage())
            .timestamp(LocalDateTime.now())
            .build();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ExternalServiceException.class)
    public ErrorResponse handleServiceException(
        ExternalServiceException ex
    ) {
        return ErrorResponse.builder()
            .code(ErrorCatalog.POKEMON_API_ERROR.getCode())
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .message(ErrorCatalog.POKEMON_API_ERROR.getMessage())
            .timestamp(LocalDateTime.now())
            .build();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponse handleException(
        Exception ex
    ) {
        return ErrorResponse.builder()
            .code(ErrorCatalog.GENERIC_ERROR.getCode())
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .message(ErrorCatalog.GENERIC_ERROR.getMessage())
            .timestamp(LocalDateTime.now())
            .build();
    }
}
