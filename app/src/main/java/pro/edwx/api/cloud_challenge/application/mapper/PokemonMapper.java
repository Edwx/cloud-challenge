package pro.edwx.api.cloud_challenge.application.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pro.edwx.api.cloud_challenge.application.dto.PokemonResponse;
import pro.edwx.api.cloud_challenge.domain.model.Pokemon;
import pro.edwx.api.cloud_challenge.infrastructure.dto.PokemonApiResponse;

import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface PokemonMapper {

    PokemonResponse toResponse(Pokemon pokemon);

    @Mapping(target = "sprite", source = "sprites.front_default")
    @Mapping(target = "types", expression = "java(mapTypes(pokemonApiResponse))")
    Pokemon toDomain(PokemonApiResponse pokemonApiResponse);

    default String mapTypes(PokemonApiResponse pokemonApiResponse) {
        return pokemonApiResponse.getTypes().stream()
            .map(t -> t.getType().getName())
            .collect(Collectors.joining(", ")
        );
    }
}
