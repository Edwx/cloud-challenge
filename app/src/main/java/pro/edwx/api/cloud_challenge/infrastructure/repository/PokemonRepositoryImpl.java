package pro.edwx.api.cloud_challenge.infrastructure.repository;

import jakarta.annotation.Resource;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import java.util.Optional;

import pro.edwx.api.cloud_challenge.application.exception.PokemonNotFoundException;
import pro.edwx.api.cloud_challenge.application.mapper.PokemonMapper;
import pro.edwx.api.cloud_challenge.domain.model.Pokemon;
import pro.edwx.api.cloud_challenge.domain.repository.PokemonRepository;
import pro.edwx.api.cloud_challenge.infrastructure.client.PokemonExternalService;
import pro.edwx.api.cloud_challenge.infrastructure.dto.PokemonApiResponse;

@Repository
@RequiredArgsConstructor
public class PokemonRepositoryImpl implements PokemonRepository {

    private final RedisTemplate<String, Pokemon> redisTemplate;
    private final PokemonExternalService pokemonExternalService;
    private final PokemonMapper pokemonMapper;

    @Value("${pokemon.id-prefix}")
    private String idPrefix;
    private static final String requestEndpoint = "pokemon";

    @Override
    public Optional<Pokemon> getPokemon(Integer id) {
        String key = idPrefix
            .concat("_")
            .concat(id.toString());

        Pokemon pokemon = redisTemplate.opsForValue().get(key);
        if (pokemon == null) {
            String endpoint = requestEndpoint
                .concat("/")
                .concat(id.toString());

            PokemonApiResponse apiResponse = pokemonExternalService
                .doGet(endpoint, null, PokemonApiResponse.class);

            if (apiResponse == null) {
                throw new PokemonNotFoundException();
            }

            pokemon = pokemonMapper.toDomain(apiResponse);
            redisTemplate.opsForValue().set(key, pokemon);
        }

        return Optional.of(pokemon);
    }
}
