package pro.edwx.api.cloud_challenge.application.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import pro.edwx.api.cloud_challenge.domain.model.Pokemon;

@Configuration
public class AppConfig {

    @Bean
    RedisTemplate<String, Pokemon> redisTemplate(
        RedisConnectionFactory factory
    ) {
        RedisTemplate<String, Pokemon> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        template.setDefaultSerializer(new JdkSerializationRedisSerializer());
        return template;
    }
}
