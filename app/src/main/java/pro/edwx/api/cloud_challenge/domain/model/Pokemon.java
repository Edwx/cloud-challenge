package pro.edwx.api.cloud_challenge.domain.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Builder
public class Pokemon implements Serializable {

    private Integer id;
    private String name;
    private String types;
    private String sprite;
}
