package pro.edwx.api.cloud_challenge.application.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PokemonResponse {

    private Integer id;
    private String name;
}
