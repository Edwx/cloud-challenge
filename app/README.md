# Web Service - Pokemon API

## Descripción

Este servicio web, desarrollado en Java utilizando Spring Boot, sigue una arquitectura limpia (Clean Architecture) y aplica principios SOLID. El servicio consume la API pública de [PokeAPI](https://pokeapi.co/) para consultar información de un Pokémon por su ID.

## Estructura del Proyecto

El proyecto está organizado de la siguiente manera:

- **application/**
  - **configuration/**: Contiene las configuraciones del proyecto.
  - **controller/**: Controladores REST que manejan las solicitudes HTTP.
  - **dto/**: Objetos de transferencia de datos.
  - **exception/**: Clases de excepciones personalizadas.
  - **mapper/**: MapStruct mappers para convertir entre DTOs y modelos de dominio.
  - **service/**: Servicios de negocio.
    - **impl/**: Implementaciones de los servicios.
- **domain/**
  - **model/**: Modelos de dominio.
  - **repository/**: Interfaces de repositorios.
- **infrastructure/**
  - **client/**: Clientes para interactuar con APIs externas.
  - **dto/**: DTOs para datos de la infraestructura.
  - **repository/**: Implementaciones de repositorios.
- **util/**: Clases utilitarias.
- **resources/**
  - **application.yml**: Archivo de configuración de Spring Boot.
  - **banner.txt**: Banner de inicio personalizado.
  - **static/**: Archivos estáticos.
  - **templates/**: Plantillas de vista.
- **test/**: Pruebas unitarias y de integración.

## Dependencias Principales

El proyecto utiliza las siguientes dependencias principales:

- **Spring Boot**: Framework principal para el desarrollo de aplicaciones Java.
- **Spring Data Redis**: Integración con Redis para almacenamiento en caché.
- **Spring Web**: Para crear servicios web RESTful.
- **Lombok**: Para reducir el boilerplate en las clases Java.
- **MapStruct**: Para mapear entre objetos DTO y modelos de dominio.
- **Micrometer Prometheus**: Para monitoreo y exposición de métricas.
- **Spring Boot Starter Validation**: Para la validación de entradas.
- **Spring Boot Actuator**: Para monitoreo y gestión de la aplicación.

## Endpoints

### Obtener información de un Pokémon

- **URL**: `/api/pokemon/{id}`
- **Método**: `GET`
- **Descripción**: Obtiene la información de un Pokémon por su ID.
- **Parámetros**:
  - `id` (int): ID del Pokémon a consultar. Debe ser mayor a 0.
- **Respuesta**: 
    ```json
    {
        "id": 1,
        "name": "bulbasaur",
        "types": "grass, poison",
        "sprite": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"
    }
    ```

## Prerrequisitos

Para ejecutar este proyecto, necesitas tener instalados los siguientes software:

- **Java 17**: JDK para compilar y ejecutar la aplicación.
- **Maven 3.6+**: Herramienta de construcción y gestión de dependencias.
- **Redis**: Base de datos en memoria utilizada para el almacenamiento en caché.