# Configuración de Kubernetes

## Descripción

Esta carpeta contiene los archivos de configuración necesarios para desplegar la arquitectura en un clúster de **Minikube**. La configuración incluye despliegues para la aplicación Spring Boot, Redis, Prometheus, Grafana, Elasticsearch, Fluent Bit y Kibana.

## Archivos de Configuración

1. **0-namespace.yml**
   - Define el namespace donde se desplegarán todos los recursos de Kubernetes.

2. **1-springboot-app-full.yml**
   - Despliegue y servicio para la aplicación Spring Boot.

3. **2-redis-cache-full.yml**
   - Despliegue y servicio para Redis como almacenamiento en caché.

4. **3-1-prometheus-full.yml**
   - Configuración para desplegar Prometheus para monitoreo.

5. **3-2-redis-exporter-full.yml**
   - Exportador de métricas para Redis, utilizado por Prometheus.

6. **3-3-grafana-full.yml**
   - Configuración para desplegar Grafana para visualización de métricas.

7. **4-1-elasticsearch-full.yml**
   - Despliegue y servicio para Elasticsearch, parte del stack EFK.

8. **4-2-fluent-bit.yml**
   - Configuración para desplegar Fluent Bit para la recolección y envío de logs a Elasticsearch.

9. **4-3-kibana-full.yml**
   - Configuración para desplegar Kibana para visualización y análisis de logs.
