# Pruebas de Carga con JMeter

## Descripción

Esta carpeta contiene el plan de pruebas utilizado para evaluar el rendimiento del proyecto bajo diferentes condiciones de carga. Las pruebas fueron realizadas utilizando Apache JMeter, versión 5.6.3.

## Archivo de Pruebas

- **cloud_challenge_test_plan.jmx**: Archivo de plan de pruebas de JMeter que contiene las configuraciones y scripts para las pruebas de carga.

## Versión de JMeter

- **Versión utilizada**: Apache JMeter 5.6.3

## Uso del Plan de Pruebas

Para ejecutar el plan de pruebas, sigue estos pasos:

### Prerrequisitos

- [Apache JMeter 5.6.3](https://jmeter.apache.org/download_jmeter.cgi) instalado.

### Procedimiento

1. **Abrir JMeter**:
   - Navega al directorio donde tienes instalado Apache JMeter.
   - Ejecuta el archivo `jmeter.bat` (Windows) o `jmeter` (Linux/Mac).

2. **Cargar el Plan de Pruebas**:
   - En la interfaz de JMeter, ve a `File > Open`.
   - Navega a la carpeta `test` de este proyecto y selecciona el archivo `cloud_challenge_test_plan.jmx`.

3. **Configurar el Plan de Pruebas**:
   - Verifica las configuraciones del plan de pruebas, tales como la URL del servicio y los parámetros de carga.
   - Ajusta cualquier configuración según sea necesario para tu entorno.

4. **Deshabilitar un Thread Group**:
   - El plan de pruebas contiene dos grupos de hilos (`Thread Groups`). Solo uno de ellos debe estar habilitado a la vez.
   - Para deshabilitar un `Thread Group`, haz clic derecho sobre el grupo de hilos en el árbol de pruebas y selecciona `Disable`.
   - Asegúrate de que el otro `Thread Group` esté habilitado seleccionando `Enable` si es necesario.

5. **Ejecutar el Plan de Pruebas**:
   - Haz clic en el botón `Start` (el botón verde con el triángulo) para iniciar la prueba de carga.
   - Observa los resultados en tiempo real en los diferentes listeners configurados en el plan de pruebas.