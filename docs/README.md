# Documentation

## Overview

Esta carpeta contiene la documentación relacionada con el proyecto 

## Files

- **documentacion_desafio_cloud.pdf**: Documento que explica el diseño y las decisiones técnicas tomadas e implementadas.
- **presentacion.pdf**: Diapositivas de la presentación.